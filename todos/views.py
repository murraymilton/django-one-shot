from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    todo_lists = TodoList.objects.all()

    context = {
        "form": form,
        "todo_item": todo_item,  # Passing the todo_item to the template
        "todo_lists": todo_lists, 
    }
    return render(request, "todos/edit_item.html", context)

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id) # TAsk has an id. task belongs to list
    else:
        form = TodoItemForm()
    todo_lists = TodoList.objects.all()
    context = {
        "form": form,
        "todo_lists": todo_lists, 
    }
    return render(request, 'todos/create_item.html', context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect('todo_list_list')  # Replace 'todo_list' with the name of your list view
    return render(request, "todos/delete.html", {'todo_list': todo_list})


  

def todo_list_update(request, id ):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todos)
        if form.is_valid():
            todos = form.save()
        return redirect('todo_list_detail', id = todos.id)
    else:
        form = TodoListForm(instance=todos)
        print(form) #Test output object

    context = {
        "form": form,
        "todo_list_object": todos
    }
    return render(request, "todos/update.html", context)




def todo_list_create (request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }

    return render(request, 'todos/create.html', context)


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/todos.html", context)

def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_object": todos
    }
    return render(request, "todos/detail.html", context)
